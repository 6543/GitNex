package org.mian.gitnex.structs;

public interface FragmentRefreshListener {

	void onRefresh(String text);

}
